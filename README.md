# docker-images

Docker images used by/for __EngIoT__ project.

### bash-curl

Needed to test _OpenHab_ Web service.

- _debian_ slim
- _curl_ 

### build-test

Needed to build and test.

- _debian_ slim
- _OpenJDK_
- _curl_
- _docker_ cli
- _docker-compose_

### theia-ide

Needed to edit, build, test and publish modifications.

- _debian_
- _node_
- _theia_ IDE
  - _terminal_
  - _git_
  - _java_
  - _json_
  - _yaml_
  - _asciidoc_
- _openJDK_
- _curl_
- _openSSL_ client
- _git_
- _telnet_
- _less_
- _docker_ cli
- _docker-compose_
- _powerline-go_
