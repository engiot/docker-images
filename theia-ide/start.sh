#!/bin/bash

set -e

eval $(ssh-agent -s)
node /srv/theia/src-gen/backend/main.js /home/theia --hostname=0.0.0.0
